﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {

    private const int maxHour = 24;
    int currentHour = 6;
    private const int maxMinute = 60;
    int currentMinute = 0;
    private int minuteInterval = 5;
    int currentDay = 1;

    private static TimeManager instance;

    public delegate void TimePassed();
    public event TimePassed OnHourPassed; //fires when an hour passed
    public event TimePassed OnMinutesPassed; //fires when minute interval has passed
    public event TimePassed OnDayPassed; //fires when a day passed

    // Use this for initialization
    void Start () {
        TickManager.OnTick += OnTick;
        if (OnHourPassed != null) OnHourPassed();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static TimeManager Instance()
    {
        if (instance == null) instance = FindObjectOfType<TimeManager>();
        return instance;
    }

    public void OnTick()
    {
//      Debug.Log("Ontick callsed");
        currentMinute += minuteInterval;

        if(currentMinute >= maxMinute)
        {
            currentHour++;
            currentMinute = 0;
            if (currentHour >= maxHour)
            {
                currentHour = 0;
                currentDay++;
                if (OnDayPassed != null) OnDayPassed();
            }
            if(OnHourPassed != null) OnHourPassed();
        }

        if(OnMinutesPassed != null) OnMinutesPassed();
    }

    public int GetCurrentHour()
    {
        return currentHour;
    }

    public int GetCurrentMinute()
    {
        return currentMinute;
    }
    
    public int GetCurrentDay()
    {
        return currentDay;
    }

    public bool IsItNight()
    {
        if (currentHour >= 20) return true;
        if (currentHour <= 6) return true;
        return false;
    }


}
