﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _TestStorageRoom : MonoBehaviour
{
    public StorageRoom storageRoomPrefab;
    public SpaceShip ship;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            MakePanels();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            InitializeShip();
        }

        if (Input.GetKeyDown(KeyCode.T)){
     //       ship.AddRoom(storageRoomPrefab);
          Test();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
          Test2();
        }
    }

    void InitializeShip()
    {
        ship.SetName("CoolShip");
        RoomTile startStorageRoomTile = ship.grid.GetRoomTiles()[0];
        SpaceShipRoom firstStorageRoom = SpaceShipRoomFactory.Instance().CreateRoom(SpaceShipRoomType.STORAGE);
        ship.AddRoom(firstStorageRoom);
        startStorageRoomTile.SetCurrentRoom(firstStorageRoom);
        ship.AddResourceAmount(ResourceType.MINERAL, 500);
        ship.AddResourceAmount(ResourceType.BIOSTUFF, 500);
        ship.AddResourceAmount(ResourceType.ENERGYSTUFF, 500);
    }

    void Test()
    {
        ship.AddResourceAmount(ResourceType.ENERGYSTUFF, 1500);
    }

    void Test2()
    {
        ship.SubstractResourceAmount(ResourceType.ENERGYSTUFF, 1500);
    }

    void MakePanels()
    {
//        UIPanelFactory.Instance().CreateStorageRoomPanel(storageRoomPrefab, 100, 100);
        UIPanelFactory.Instance().CreateShipStatsPanel(ship, 400, 100);
    }

    string StorageString(ResourceType resourceType)
    {
        string s = "";
        s += resourceType.ToString();
        s += ": ";
        s += storageRoomPrefab.GetResourceAmountStored(resourceType);
        s += "/";
        s += storageRoomPrefab.GetResourceLimit(resourceType);

        return s;
    }

    void OutputStorageData()
    {
        Debug.Log(StorageString(ResourceType.BIOSTUFF));
        Debug.Log(StorageString(ResourceType.ENERGYSTUFF));
        Debug.Log(StorageString(ResourceType.MINERAL));
    }
}
