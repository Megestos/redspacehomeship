﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageRoom : SpaceShipRoom
{
    private Dictionary<ResourceType, int> storedResources;
    private Dictionary<ResourceType, int> resourceLimit;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        Init();
    }

    private void Init()
    {
        Debug.Log("StorageRoom Init");
        base.roomtype = SpaceShipRoomType.STORAGE;
        base.roomName = roomtype.ToString();
        storedResources = new Dictionary<ResourceType, int>();
        storedResources.Add(ResourceType.BIOSTUFF, 0);
        storedResources.Add(ResourceType.ENERGYSTUFF, 0);
        storedResources.Add(ResourceType.MINERAL, 0);
        resourceLimit = new Dictionary<ResourceType, int>(); //#TODO this should be taken from a Balancing Object
        resourceLimit.Add(ResourceType.BIOSTUFF, 1000);
        resourceLimit.Add(ResourceType.ENERGYSTUFF, 1000);
        resourceLimit.Add(ResourceType.MINERAL, 1000);
    }

    public int GetResourceLimit(ResourceType resourceType)
    {
        return resourceLimit[resourceType];
    }

    public int GetResourceAmountStored(ResourceType resourceType)
    {
        return storedResources[resourceType];
    }

    public int GetResourceAvailableSpace(ResourceType resourceType)
    {
        return (GetResourceLimit(resourceType) - GetResourceAmountStored(resourceType));
    }

    public void AddResource(ResourceType resourceType, int amount)
    {
        storedResources[resourceType] += amount;
        if (storedResources[resourceType] > GetResourceLimit(resourceType)) storedResources[resourceType] = GetResourceLimit(resourceType);
    }

    public void SubstractResource(ResourceType resourceType, int amount)
    {
        storedResources[resourceType] -= amount;
        if (storedResources[resourceType] < 0)
        {
            throw new UnityException("Substracting " + amount + " " + resourceType.ToString() + " would reduce Amount below 0");
        }
    }
}
