﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipRoomFactory : MonoBehaviour
{
    public static SpaceShipRoomFactory instance;

    public static SpaceShipRoomFactory Instance()
    {
        if (instance == null) instance = FindObjectOfType<SpaceShipRoomFactory>();
        return instance;
    }

    public StorageRoom storageRoomPrefab;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private StorageRoom CreateStorageRoom()
    {
        StorageRoom stroom = Instantiate(storageRoomPrefab);
        return stroom;
    }

    public SpaceShipRoom CreateRoom(SpaceShipRoomType roomType)
    {
        if (roomType == SpaceShipRoomType.STORAGE) return CreateStorageRoom();

        throw new System.Exception("Creating Room Type not Implemented: " + roomType);
    }
}
