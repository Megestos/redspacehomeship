﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]

public class RoomTile : MonoBehaviour
{
    public SpriteRenderer roomSprite;
    public Color normalColor;
    public Color mouseOverColor;
    public TMPro.TextMeshPro tLetter;

    public Sprite emptySprite;

    public Vector2 coordinates;
    public float spriteSize = 0.42f;

    private SpaceShipRoom currentRoom = null;

    private RoomTileGrid grid;


    // Start is called before the first frame update
    void Start()
    {
        AssumePosition();
        DisplayEmpty();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter()
    {
        roomSprite.color = mouseOverColor;
    }

    private void OnMouseExit()
    {
        roomSprite.color = normalColor;
    }

    private void OnMouseDown()
    {
        if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            LeftClickEvent();
        }

    }

    private void LeftClickEvent()
    {
        grid.SetSelectedTile(this);

        if (currentRoom != null)
        {

            switch (currentRoom.roomtype)
            {
                case SpaceShipRoomType.STORAGE:
                    UIPanelFactory.Instance().CreateStorageRoomPanel((StorageRoom)currentRoom, 100, 100);
                break;
            }
        }
        else
        {
            UIPanelFactory.Instance().CreateBuildMenuPanel(grid.GetSpaceShip(), 100, 100);
        }

    }


    private void AssumePosition()
    {
        transform.localPosition = new Vector3(coordinates.x * spriteSize, coordinates.y * spriteSize, 0);
    }

    private void DisplayEmpty()
    {
        tLetter.text = "";
        roomSprite.sprite = emptySprite;
    }

    public void DisplayRoom(SpaceShipRoom spaceShipRoom)
    {
        switch (spaceShipRoom.roomtype)
        {
            case SpaceShipRoomType.STORAGE:
            tLetter.text = "S";
            break;
        }
    }

    public void SetCurrentRoom(SpaceShipRoom spaceShipRoom)
    {
        currentRoom = spaceShipRoom;
        DisplayRoom(spaceShipRoom);
    }

    public SpaceShipRoom GetCurrentRoom()
    {
        return currentRoom;
    }

    public void ClearCurrentRoom()
    {
        currentRoom = null;
        DisplayEmpty();
    }

    public void SetGrid(RoomTileGrid roomTileGrid)
    {
        grid = roomTileGrid;
    }
}
