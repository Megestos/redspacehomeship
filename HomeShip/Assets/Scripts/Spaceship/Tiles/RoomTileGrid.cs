﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTileGrid : MonoBehaviour
{
    private List<RoomTile> roomTiles;
    public RoomTile roomTilePrefab;
    public Vector2[] coordinatesInGrid;
    public GameObject shipSprite;

    private RoomTile _selectedTile;

    private SpaceShip ship;

    // Start is called before the first frame update
    void Start()
    {
        ;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<RoomTile> GetRoomTiles()
    {
        return roomTiles;
    }

    public void MakeGrid()
    {
        roomTiles = new List<RoomTile>();
        foreach(Vector2 co in coordinatesInGrid)
        {
            RoomTile rt;
            rt = Instantiate(roomTilePrefab);
            rt.name = ("RoomTile" + co.x + "/" + co.y);
            rt.coordinates = co;
            rt.transform.SetParent(shipSprite.transform, false);
            rt.SetGrid(this);
            roomTiles.Add(rt);
        }

    }

    public RoomTile GetSelectedTile()
    {
        return _selectedTile;
    }

    public void SetSelectedTile(RoomTile selectedTile)
    {
        _selectedTile = selectedTile;
    }

    
    public void SetSpaceShip(SpaceShip spaceShip)
    {
        ship = spaceShip;
    }

    public SpaceShip GetSpaceShip()
    {
        return ship;
    }
    
}
