﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{

    private string _name = "Unknown";
    private List<SpaceShipRoom> roomList;

    public RoomBuilder roomBuilder;
    public RoomTileGrid grid;
    

    // Start is called before the first frame update
    void Start()
    {
        roomList = new List<SpaceShipRoom>();
        roomBuilder.SetSpaceShip(this);
        grid.SetSpaceShip(this);

        //Setting up First Ship; should be moved in a ShipMaker Class #TODO
        grid.MakeGrid();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetName(string name)
    {
        _name = name;
    }

    public string GetName()
    {
        return _name;
    }

    public void AddRoom(SpaceShipRoom room)
    {
        roomList.Add(room);
    }

    public void RemoveRoom(SpaceShipRoom room)
    {
        roomList.Remove(room);
    }

    public SpaceShipRoom[] GetRooms()
    {
        return roomList.ToArray();
    }

    public SpaceShipRoom[] GetRoomsOfType(SpaceShipRoomType type)
    {
        List<SpaceShipRoom> tempList = new List<SpaceShipRoom>();
        foreach(SpaceShipRoom room in roomList)
        {
            if (room.roomtype == type)
            {
                tempList.Add(room);
            }
        }
        return tempList.ToArray();
    }

    private List<StorageRoom> GetStorageRooms()
    {
        SpaceShipRoom[] rooms = GetRoomsOfType(SpaceShipRoomType.STORAGE);
        List<StorageRoom> storageList = new List<StorageRoom>();
        foreach (SpaceShipRoom room in rooms)
        {
            storageList.Add((StorageRoom)room);
        }
        return storageList;
    }

    public Dictionary<ResourceType, int> GetShipStorage()
    {
        Dictionary<ResourceType, int> tempStore = new Dictionary<ResourceType, int>();
        List<StorageRoom> storageRooms = GetStorageRooms();
        int energy = 0;
        int mineral = 0;
        int biostuff = 0;
        foreach (StorageRoom storageR in storageRooms) // #TODO iterate over enums instead
        {
            energy += storageR.GetResourceAmountStored(ResourceType.ENERGYSTUFF);
            mineral += storageR.GetResourceAmountStored(ResourceType.MINERAL);
            biostuff += storageR.GetResourceAmountStored(ResourceType.BIOSTUFF);
        }
        tempStore.Add(ResourceType.ENERGYSTUFF, energy);
        tempStore.Add(ResourceType.MINERAL, mineral);
        tempStore.Add(ResourceType.BIOSTUFF, biostuff);
        return tempStore;
    }

    public int GetResourceAmount(ResourceType resourceType)
    {
        List<StorageRoom> storageRooms = GetStorageRooms();
        int amount = 0;
        foreach (StorageRoom storageR in storageRooms)
        {
            amount += storageR.GetResourceAmountStored(resourceType);
        }
        return amount;
    }

    //Adds the Specified Resource the Ship's Storage. Distributes it automatically among Rooms. !Assumes that there is enough Storage Room!
    public void AddResourceAmount(ResourceType resourceType, int amount)
    {
        List<StorageRoom> storageRooms = GetStorageRooms();
        foreach (StorageRoom storageR in storageRooms)
        {
            int space = storageR.GetResourceAvailableSpace(resourceType);
            if (space < amount)
            {
                storageR.AddResource(resourceType, amount);
                amount -= space;
            }
            else
            {
                storageR.AddResource(resourceType, amount);
                return;
            }
        }
    }

    //Adds the Specified Resource the Ship's Storage. Distributes it automatically among Rooms. !Assumes that there is enough of the Resource in Storage!
    public void SubstractResourceAmount(ResourceType resourceType, int amount)
    {
        List<StorageRoom> storageRooms = GetStorageRooms();
        foreach (StorageRoom storageR in storageRooms)
        {
            int rStored = storageR.GetResourceAmountStored(resourceType);
            if (rStored < amount)
            {
                storageR.SubstractResource(resourceType, rStored);
                amount -= rStored;
            }
            else
            {
                storageR.SubstractResource(resourceType, amount);
                return;
            }
        }
        throw new System.Exception("Cannot substract Resource from Ship: " + _name + "," + resourceType.ToString() + "," + amount);
    }
}
