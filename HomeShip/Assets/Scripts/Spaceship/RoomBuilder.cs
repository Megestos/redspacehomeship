﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomBuilder : MonoBehaviour
{

    public RoomData roomData;
    private SpaceShip ship;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSpaceShip(SpaceShip spaceShip)
    {
        ship = spaceShip;
    }

    public void BuildRoom(SpaceShipRoomType roomType, RoomTile roomTile)
    {
        if (!CanBuildRoom(roomType, roomTile)) throw new System.Exception("Trying to build Room that cannot be built: " + roomType + "," + roomTile.ToString());

        ship.SubstractResourceAmount(ResourceType.MINERAL, roomData.getMineralCost(roomType));
        SpaceShipRoom newRoom = SpaceShipRoomFactory.Instance().CreateRoom(roomType);
        ship.AddRoom(newRoom);
        roomTile.SetCurrentRoom(newRoom);
    }

    public bool CanBuildRoom(SpaceShipRoomType roomType, RoomTile roomTile)
    {
        if (roomTile.GetCurrentRoom() != null) return false;
        else if (roomData.getMineralCost(roomType) > ship.GetResourceAmount(ResourceType.MINERAL)) return false;
        else return true;
    }
}
