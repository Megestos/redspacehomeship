﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickManager : MonoBehaviour {

	public delegate void Tick();
	public static event Tick OnTick; //Actions that happen in the middle of a Tick
	public static event Tick EndTick; //Actions that happen at the End of a Tick

	public float tickFrequencySlow = 3f; //tickingMode 1
	public float tickFrequencyMedium = 1f; //tickingMode 2
	public float tickFrequencyFast = 0.3f; //tickingMode 3
	public float tickFrequencyUltraFast = 0.1F; //tickingMode 4
	private float tickCount = 0f;
	int tickingMode = 0; //tickingMode 0 = stopped

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (tickingMode>0) {
			tickCount += Time.deltaTime;
//			Debug.Log ("Tick Count: " + tickCount);
//			Debug.Log ("Tick Frequency: " + tickFrequency (tickingMode));
			if (tickCount >= tickFrequency (tickingMode)) {
				DoTick ();
				tickCount = 0f;
			}
		}
//		TestTest ();
	} 

	private void DoTick(){
		if (OnTick != null) OnTick ();
		if (EndTick != null) EndTick ();
	}

	private void TestTest(){
		if (Input.GetKeyDown (KeyCode.Period)) {
			Debug.Log ("TickManager makes a tick");
			DoTick ();
		}
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			startAutotickSlow ();
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			startAutotickMedium ();
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			startAutotickFast();
		}
		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			startAutotickUltraFast();
		}
		if (Input.GetKeyDown (KeyCode.Alpha0)) {
			stopAutotick ();;
		}
	}

	/*
	 * Starts Autoticking with the specified speed (1 = slow, 2 = medium, 3 = false)
	 */ 
	public void startAutotick(int speed){
		if ((speed < 1) || (speed > 4)) {
			throw new UnityException ("Illegal Speed Mode: " + speed + "; must be 1-4");
		}
		tickingMode = speed;
		tickCount = 0f;
	}

	public void stopAutotick(){
		tickingMode = 0;
		Debug.Log ("TickManager set to Pause");
	}

	public void startAutotickSlow (){
		tickingMode = 1;
		Debug.Log ("TickManager set to Slow Ticking");
	}

	public void startAutotickMedium (){
		tickingMode = 2;
		Debug.Log ("TickManager set to Medium Ticking");
	}

	public void startAutotickFast (){
		tickingMode = 3;
		Debug.Log ("TickManager set to Fast Ticking");
	}
	public void startAutotickUltraFast (){
		tickingMode = 4;
		Debug.Log ("TickManager set to Ultra Ticking");
	}

	private float tickFrequency(int speed){
		if (speed == 1)
			return tickFrequencySlow;
		if (speed == 2)
			return tickFrequencyMedium;
		if (speed == 3)
			return tickFrequencyFast;
		if (speed == 4)
			return tickFrequencyUltraFast;
		return -1; //TODO add Proper Exception?
	}



}
