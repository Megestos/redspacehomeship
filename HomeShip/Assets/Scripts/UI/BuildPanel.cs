﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildPanel : MoveablePanel
{

    public Dropdown dropDown;
    public SpaceShip spaceShip;
    SpaceShipRoomType[] roomOptions;
    public TMPro.TextMeshProUGUI titleText;
    public TMPro.TextMeshProUGUI buildButtonText;

    // Start is called before the first frame update
    void Start()
    {
        roomOptions = (SpaceShipRoomType[])System.Enum.GetValues(typeof(SpaceShipRoomType));
        AddRoomTypeOptions();
        SelectEvent();
    }

    // Update is called once per frame
    void Update()
    {
        if (spaceShip.grid.GetSelectedTile() != null) titleText.text = ("Build " + spaceShip.grid.GetSelectedTile().coordinates); //#TODO this shouldnt be in every Update ;)
    }

    private void AddRoomTypeOptions()
    {
        
        dropDown.ClearOptions();
        List<string> stringList = new List<string>();
        for (int i = 0; i < roomOptions.Length; i++)
        {
            stringList.Add(roomOptions[i].ToString());
        }
        dropDown.AddOptions(stringList);
    }

    public void SelectEvent()
    {
        SpaceShipRoomType selectedType = roomOptions[dropDown.value];
        buildButtonText.text = spaceShip.roomBuilder.roomData.getMineralCost(selectedType).ToString() + " (M)";
    }

    public void BuildEvent()
    {
        RoomTile rt = spaceShip.grid.GetSelectedTile();
        SpaceShipRoomType selectedType = roomOptions[dropDown.value];

        if (spaceShip.roomBuilder.CanBuildRoom(selectedType, rt))
        {
            spaceShip.roomBuilder.BuildRoom(selectedType, spaceShip.grid.GetSelectedTile());
            Close();
        }
        else
        {
            Debug.LogError("Cannot build Storage Room here");
        }
    }
}
