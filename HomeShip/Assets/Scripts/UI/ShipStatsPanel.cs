﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShipStatsPanel : MoveablePanel
{
    public TextMeshProUGUI energyText;
    public TextMeshProUGUI mineralsText;
    public TextMeshProUGUI biostuffText;

    private SpaceShip _spaceShip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_spaceShip != null) Refresh();
    }

    public void SetSpaceShip(SpaceShip spaceShip)
    {
        _spaceShip = spaceShip;
        Refresh();
    }

    private void Refresh()
    {
        Dictionary<ResourceType, int> storage = _spaceShip.GetShipStorage();
        energyText.text = storage[ResourceType.ENERGYSTUFF].ToString();
        mineralsText.text = storage[ResourceType.MINERAL].ToString();
        biostuffText.text = storage[ResourceType.BIOSTUFF].ToString();
    }
}
