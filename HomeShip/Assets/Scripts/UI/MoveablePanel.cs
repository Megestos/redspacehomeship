﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveablePanel : MonoBehaviour, IDragHandler
{
    private List<MoveablePanel> tiedPanels;
    public bool moveable = true;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    protected void Awake()
    {
        tiedPanels = new List<MoveablePanel>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        ReceiveDrag(eventData.delta);
        if (tiedPanels.Count > 0)
        {
            foreach (MoveablePanel mp in tiedPanels)
            {
                mp.ReceiveDrag(eventData.delta);
            }
        }
    }

    public void ReceiveDrag(Vector2 v2)
    {
        if(moveable)transform.Translate(v2);
    }

    public void Close()
    {
        GameObject.Destroy(gameObject);
        foreach (MoveablePanel mp in tiedPanels)
        {
            mp.Untie(this);
            mp.Close();
        }
    }

    public void TieWith(MoveablePanel mp)
    {
        ReceiveTie(mp);
        mp.ReceiveTie(this);
    }

    public void ReceiveTie(MoveablePanel mp)
    {
        tiedPanels.Add(mp);
    }

    public void Untie(MoveablePanel mp)
    {
        tiedPanels.Remove(mp);
    }
}
