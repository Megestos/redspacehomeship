﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpaceShipRoomHeadPanel : MoveablePanel
{

    public TextMeshProUGUI titleText;
    private SpaceShipRoom _room;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSpaceShipRoom(SpaceShipRoom room)
    {
        _room = room;
        titleText.text = _room.roomName;
    }
}
