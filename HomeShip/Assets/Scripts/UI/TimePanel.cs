﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimePanel : MonoBehaviour {

    public Button pauseButton;
    public Button speed1Button;
    public Button speed2Button;
    public Button speed3Button;
    public Color neutralColor = Color.white;
    public Color selectedColorSpeed;
    public Color selectedColorPause;
    public Text timeText;
    public TickManager tickManager;


	// Use this for initialization
	void Start () {
        pauseButton.GetComponent<Image>().color = neutralColor;
        speed1Button.GetComponent<Image>().color = neutralColor;
        speed2Button.GetComponent<Image>().color = neutralColor;
        speed3Button.GetComponent<Image>().color = neutralColor;
        TimeManager.Instance().OnHourPassed += OnHour;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateTimeText();
        if (Input.GetKeyDown(KeyCode.Space)) SetSpeed(0);
        if (Input.GetKeyDown(KeyCode.Alpha1)) SetSpeed(1);
        if (Input.GetKeyDown(KeyCode.Alpha2)) SetSpeed(2);
        if (Input.GetKeyDown(KeyCode.Alpha3)) SetSpeed(3);
    }

    void UpdateTimeText()
    {
        int minutes = TimeManager.Instance().GetCurrentMinute();
        string minString;
        if (minutes < 10) minString = "0" + minutes.ToString();
        else minString = minutes.ToString();

        int hours = TimeManager.Instance().GetCurrentHour();
        string hourString;
        if (hours < 10) hourString = "0" + hours.ToString();
        else hourString = hours.ToString();

        string stri = hourString + ":" + minString;
        timeText.text = stri;
    }

    public void SetSpeed(int speed)
    {
        if ((speed < 0) || speed > 3) throw new UnityException("Cannot set speed because no such speed: " + speed);
        switch (speed)
        {
            case 0:
                pauseButton.GetComponent<Image>().color = selectedColorPause;
                speed1Button.GetComponent<Image>().color = neutralColor;
                speed2Button.GetComponent<Image>().color = neutralColor;
                speed3Button.GetComponent<Image>().color = neutralColor;
                tickManager.stopAutotick();
                break;
            case 1:
                pauseButton.GetComponent<Image>().color = neutralColor;
                speed1Button.GetComponent<Image>().color = selectedColorSpeed;
                speed2Button.GetComponent<Image>().color = neutralColor;
                speed3Button.GetComponent<Image>().color = neutralColor;
                tickManager.startAutotickSlow();
                break;
            case 2:
                pauseButton.GetComponent<Image>().color = neutralColor;
                speed1Button.GetComponent<Image>().color = selectedColorSpeed;
                speed2Button.GetComponent<Image>().color = selectedColorSpeed;
                speed3Button.GetComponent<Image>().color = neutralColor;
                tickManager.startAutotickMedium();
                break;
            case 3:
                pauseButton.GetComponent<Image>().color = neutralColor;
                speed1Button.GetComponent<Image>().color = selectedColorSpeed;
                speed2Button.GetComponent<Image>().color = selectedColorSpeed;
                speed3Button.GetComponent<Image>().color = selectedColorSpeed;
                tickManager.startAutotickFast();
                break;
        }
    }

    public void OnHour()
    {
        int hours = TimeManager.Instance().GetCurrentHour();
    }
}
