﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelFactory : MonoBehaviour
{

    public Canvas canvas;

    private static UIPanelFactory instance;
    public ShipStatsPanel shipStatsPanelPrefab;
    public SpaceShipRoomHeadPanel spaceShipRoomHeadPanelPrefab;
    public StorageRoomPanel storageRoomPanelPrefab;
    public BuildPanel buildPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static UIPanelFactory Instance()
    {
        if (instance == null) instance = FindObjectOfType<UIPanelFactory>();
        return instance;
    }

    private SpaceShipRoomHeadPanel _CreateSpaceShipRoomHeadPanel(SpaceShipRoom room, int x, int y)
    {
        SpaceShipRoomHeadPanel headP = Instantiate(spaceShipRoomHeadPanelPrefab);
        headP.SetSpaceShipRoom(room);
        headP.transform.SetParent(canvas.transform);
        headP.transform.localPosition = new Vector3(x, y, 0);
        return headP;
    }

    private StorageRoomPanel _CreateStorageRoomBodyPanel(StorageRoom storageRoom)
    {
        StorageRoomPanel srp = Instantiate(storageRoomPanelPrefab);
        srp.SetSpaceShipRoom(storageRoom);
        return srp;
    }

    public SpaceShipRoomHeadPanel CreateStorageRoomPanel(StorageRoom storageRoom, int x, int y)
    {
        SpaceShipRoomHeadPanel headP = _CreateSpaceShipRoomHeadPanel(storageRoom, x, y);
        StorageRoomPanel srp = _CreateStorageRoomBodyPanel(storageRoom);
        srp.transform.SetParent(headP.transform, false);
        srp.moveable = false;
        //   srp.transform.localPosition.Set(0, 0, 0);
        return headP;
    }

    public BuildPanel CreateBuildMenuPanel(SpaceShip spaceShip, int x, int y)
    {
        BuildPanel bupa = Instantiate(buildPanel);
        bupa.transform.SetParent(canvas.transform);
        bupa.transform.localPosition = new Vector3(x, y, 0);
        bupa.spaceShip = spaceShip;
        return bupa;
    }

    public ShipStatsPanel CreateShipStatsPanel(SpaceShip spaceShip, int x, int y)
    {
        ShipStatsPanel statsP = Instantiate(shipStatsPanelPrefab);
        statsP.transform.SetParent(canvas.transform);
        statsP.transform.localPosition = new Vector3(x, y, 0);
        statsP.SetSpaceShip(spaceShip);
        return statsP;
    }

}
