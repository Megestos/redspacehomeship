﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class StorageRoomPanel : SpaceShipRoomBodyPanel
{

    public TextMeshProUGUI eTitle;
    public TextMeshProUGUI eAmount;
    public TextMeshProUGUI mTitle;
    public TextMeshProUGUI mAmount;
    public TextMeshProUGUI bTitle;
    public TextMeshProUGUI bAmount;
    private StorageRoom _storageRoom;

    // Start is called before the first frame update
    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        Refresh();
    }

    private void Refresh()
    {
        eTitle.text = "Energy";
        eAmount.text = StorageAmountString(ResourceType.ENERGYSTUFF);
        mTitle.text = "Minerals";
        mAmount.text = StorageAmountString(ResourceType.MINERAL);
        bTitle.text = "Biostuff";
        bAmount.text = StorageAmountString(ResourceType.BIOSTUFF);
    }

    public void SetSpaceShipRoom(StorageRoom storageRoom)
    {
        base.SetSpaceShipRoom(storageRoom);
        _storageRoom = storageRoom;
        Refresh();
    }

    string StorageAmountString(ResourceType resourceType)
    {
        string s = "";
        s += _storageRoom.GetResourceAmountStored(resourceType);
        s += "/";
        s += _storageRoom.GetResourceLimit(resourceType);

        return s;
    }
}
