﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObjects", order = 1)]
public class RoomData : ScriptableObject
{
    [System.Serializable]
    public struct RoomD
    {
        public SpaceShipRoomType roomtype;
        public int mineralCost;
    }

    public RoomD[] roomDatas;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int getMineralCost(SpaceShipRoomType roomType)
    {
        foreach(RoomD rd in roomDatas)
        {
            if (rd.roomtype == roomType) return rd.mineralCost;
        }
        throw new System.Exception("No Mineral Cost for Room Type " + roomType);
    }
}
